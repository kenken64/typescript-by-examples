let notSure: any = 4;
notSure = "maybe a string instead";
let xhello: string = "ppddd";
console.log(typeof xhello);
console.log(notSure);
console.log(" >> " + typeof notSure);
notSure = false;
console.log(notSure);

/*
let notSure2: any=4;
notSure2.ifItExist();
notSure2.toFixed();

let prettySure: Object = 4;
prettySure.toFixed();
*/

let list: any[] = [1, true, "free"];
list[1] = 100;
console.log(list[1]);
