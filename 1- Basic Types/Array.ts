let list: number[] = [1,2,3];

list.forEach(function(element, index){
    console.log("Index: " + index);
    console.log("Element : " + element);
});

let list2: boolean[] = [true,false,true];
list2.forEach(function(element, index){
    console.log("Index: " + index);
    console.log("Element : " + element);
});

let list3: string[] = ["real","not real","super real"];
list3.forEach(function(element, index){
    console.log("Index: " + index);
    console.log("Element : " + element);
});

// generic array 
let list4: Array<number> = [1, 2, 3];
console.log("list4: " + list4);
