function error(message: string): never {
    throw new Error(message);
}

function fail(){
    return error("Something failed !");
}

console.log(error("hello world !"));
console.log(fail());
