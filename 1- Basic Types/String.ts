let colour: string = "blue";
console.log(colour);
colour = 'red';
console.log(colour);

let fullName: string = 'Kenneth Phang';

let age: number = 37;
let sentence: string = `Hello, my name is ${ fullName }. 
I\'ll be ${age + 1} years old next month`;
console.log(sentence);