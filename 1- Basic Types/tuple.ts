let x: [number, string, boolean];
x = [10, "Hello", false];
console.log(x);
// x = ["10", 20, false]; // cant do this
console.log(x[1].substr(1)); // OK
// console.log(x[0].substr(1));
x[3] =  'World!';
console.log(x[3].toString());
x[4] = true;
console.log("item 4 " + x[4]);
