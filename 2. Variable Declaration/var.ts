var a = 10;
function f(){
    var message = "Hello, World!";
    return message;
}

function f2(){
    var a = 10;
    return function g(){
        var b = a+1;
        return b;
    }
}

console.log(f());
console.log(a);
var g = f2();
console.log(g());

function f3(){
    var a=1;
    a =2;
    var b = g();
    a = 3;
    return b;

    function g(){
        return a;
    }
}

console.log(f3());

function f4(shouldInitialize: boolean){
    if(shouldInitialize){
        var x = 10;
    }
    return x;
}

console.log(f4(true));
console.log(f4(false));



